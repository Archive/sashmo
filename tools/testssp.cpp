#include <gtk/gtk.h>
#include <gnome.h>
#include "SashSecurityPane.h"

using namespace std;

void print(gpointer key, gpointer val, gpointer me) {
	 char* c = (char*) key;
	 SecurityItem* i = (SecurityItem*) val;
	 cout << c << ": " << i->m_DescriptiveName << " --> ";
	 if (i->m_Type == ST_BOOL) {
		  cout << (i->m_Bool ? "true" : "false") << endl;
	 } else if (i->m_Type == ST_STRING_ENUM) {
		  cout << i->m_StringVals[i->m_StringIndex] << endl;
	 } else if (i->m_Type == ST_NUMBER) {
		  cout << i->m_Number << endl;
	 } else {
		  cout << i->m_String << endl;
	 }
}

int main(int argc, char *argv[]) {
  GHashTable *h;
  h= g_hash_table_new(g_str_hash, g_str_equal);
  
  SecurityItem seca = SecurityItem();
  seca.m_DescriptiveName= "Global";
  seca.m_ID = -1;

  SecurityItem secb = SecurityItem();
  secb.m_DescriptiveName= "Registry";
  secb.m_ID = -1;

  SecurityItem sec1 = SecurityItem();
  sec1.m_Parent= "Global";
  sec1.m_DescriptiveName= "Allow filesystem access";

  SecurityItem sec2 = SecurityItem();
  sec2.m_Parent= "Global";
  sec2.m_DescriptiveName= "User name";
  sec2.m_Type= ST_STRING;
  sec2.m_String= "GOD";

  SecurityItem sec3 = SecurityItem();
  sec3.m_Parent= "Registry";
  sec3.m_DescriptiveName= "Access rights granted";
  sec3.m_Type= ST_STRING_ENUM;
  sec3.m_StringVals.push_back("Hula-Hula");
  sec3.m_StringVals.push_back("Go-Go");
  sec3.m_StringVals.push_back("So-So");
  sec3.m_StringVals.push_back("NO!NO!");
  sec3.m_StringIndex= 1;

  SecurityItem sec4 = SecurityItem();
  sec4.m_Parent= "Registry";
  sec4.m_DescriptiveName= "BSOD Probability";
  sec4.m_Type= ST_NUMBER;
  sec4.m_Number= 1.01;
  
  SecurityItem sec5 = sec4;
  sec5.m_Parent = "Global";

  g_hash_table_insert(h, (void *) "Global\\-1", (void *) &seca);
  g_hash_table_insert(h, (void *) "Registry\\-1", (void *) &secb);
  g_hash_table_insert(h, (void *) "key1", (void *) &sec1);
  g_hash_table_insert(h, (void *) "key2", (void *) &sec2);
  g_hash_table_insert(h, (void *) "key3", (void *) &sec3);
  g_hash_table_insert(h, (void *) "key4", (void *) &sec4);
  g_hash_table_insert(h, (void *) "key5", (void *) &sec5);

   gnome_init("Test SSP", "0.0", argc, argv);
  
   sash_security_pane_dialog(h, 500, 300);

   g_hash_table_foreach(h, print, NULL);
  
  return 0;
}
