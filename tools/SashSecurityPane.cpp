// Stefan Atev, AJ Shankar

#include "SashSecurityPane.h"
#include "SecurityItem.h"
#include <debugmsg.h>
#include <iostream.h>
using namespace std;

static void sash_security_pane_class_init(SashSecurityPaneClass *klass) {

}

static void sash_security_pane_init(SashSecurityPane *ssp) {
  ssp->scroll_pane= gtk_scrolled_window_new(NULL, NULL);
  ssp->tree= gtk_tree_new();
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(ssp->scroll_pane), ssp->tree);
  gtk_widget_show(ssp->tree);
  gtk_container_add(GTK_CONTAINER(ssp), ssp->scroll_pane);
  gtk_widget_show(ssp->scroll_pane);
}

guint sash_security_pane_get_type() {
  static guint ssp_type= 0;
  
  if (ssp_type== 0) {
    GtkTypeInfo ssp_info= {
      "SashSecurityPane",
      sizeof(SashSecurityPane),
      sizeof(SashSecurityPaneClass),
      (GtkClassInitFunc) sash_security_pane_class_init,
      (GtkObjectInitFunc) sash_security_pane_init,
      (void *) NULL,
      (void *) NULL
    };
    ssp_type= gtk_type_unique(gtk_vbox_get_type(), &ssp_info);
  }
  return ssp_type;
}

GtkWidget* sash_security_pane_new() {
  return GTK_WIDGET(gtk_type_new(sash_security_pane_get_type()));
}

void focus_grabber(GtkItem *item, gpointer extra) {
  if (extra!= NULL) {
    if (GTK_IS_COMBO(extra))
      gtk_widget_grab_focus(GTK_WIDGET(GTK_COMBO(extra)->entry));
//      gtk_combo_popup_list(GTK_COMBO(extra));
    else
      gtk_widget_grab_focus(GTK_WIDGET(extra));
  }
}

void prohibit_tree_selection(GtkTree *tree, GtkWidget *item, gpointer extra) {
  gtk_tree_unselect_child(tree, item);
}

GtkTreeItem *get_tree_item_by_name(GtkTree *tree, gchar *name) {
  GList *list, *child_list;
  gchar *child_label;
  
  list= gtk_container_children(GTK_CONTAINER(tree));
  while (list!= NULL) {
    child_list= gtk_container_children(GTK_CONTAINER(list->data));
    gtk_label_get(GTK_LABEL(child_list->data), &child_label);
    if (strcasecmp(child_label, name)== 0)
      return GTK_TREE_ITEM(list->data);
    list= g_list_next(list);
  }
  return NULL;
}

GtkWidget *get_appropriate_widget(SecurityItem *item) {
  GtkWidget *ret;
  gchar *text;
  GList *list;
  int i;
  
  switch (item->m_Type) {
    case ST_BOOL:
      ret= gtk_check_button_new_with_label(item->m_DescriptiveName.c_str());
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ret), (item->m_Bool ? TRUE : FALSE));
      break;
    case ST_STRING:
      ret= gtk_entry_new();
      gtk_entry_set_text(GTK_ENTRY(ret), (gchar *) item->m_String.c_str());
      break;
    case ST_STRING_ENUM:
      ret= gtk_combo_new();
      list= NULL;
      for (i= item->m_StringVals.size()- 1; i>= 0; --i) 
        list= g_list_prepend(list, g_strdup((gchar *) item->m_StringVals[i].c_str()));
      gtk_combo_set_popdown_strings(GTK_COMBO(ret), list);
      gtk_combo_set_value_in_list(GTK_COMBO(ret), TRUE, FALSE);
      text= g_strdup((gchar *) item->m_StringVals[item->m_StringIndex].c_str()); 
      gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(ret)->entry), text);
      delete [] text;
      gtk_entry_set_editable(GTK_ENTRY(GTK_COMBO(ret)->entry), FALSE);
      while (list!= NULL) {
        g_free(list->data);
        list= g_list_next(list);
      }
      break;
    case ST_NUMBER:
      ret= gtk_spin_button_new(
        GTK_ADJUSTMENT(gtk_adjustment_new(item->m_Number, -FLT_MAX, FLT_MAX, 1.0, 0.0, 0.0)), 1.0, 3);
      gtk_spin_button_set_value(GTK_SPIN_BUTTON(ret), item->m_Number);
      gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(ret), TRUE);
      gtk_spin_button_set_update_policy(GTK_SPIN_BUTTON(ret), GTK_UPDATE_IF_VALID);
      break;
    default:
      ret= gtk_label_new("Undefined type of security attribute");
  }
  return ret;
}

void insert_security_item(gpointer key, gpointer value, gpointer ssp) {
  GtkWidget *item_label, *item_widget, *item_container, *sub_tree, *entry_item;
  GtkTreeItem *section_item;
  gchar *parent_name;
  // to find the parent, we have to look up its special value in the hash
  // if we have the special value itself, ignore it
  if (((SecurityItem*) value)->m_ID == strtod(SECURITY_KEY_DESCRIP.c_str(), NULL)) return;
  GHashTable* g = SASH_SECURITY_PANE(ssp)->entry_hash;
  string lookup_string = ((SecurityItem *) value)->m_Parent + "\\" + SECURITY_KEY_DESCRIP;
  SecurityItem* i = (SecurityItem*) g_hash_table_lookup(g, lookup_string.c_str());
  if (i) {
	   parent_name = (gchar*) i->m_DescriptiveName.c_str();
  } else {
	   return;
  }

  item_widget= get_appropriate_widget((SecurityItem *) value);
  item_label= gtk_label_new((gchar *) ((SecurityItem *) value)->m_DescriptiveName.c_str());
  gtk_misc_set_alignment(GTK_MISC(item_label), 0.0, 0.5);
  
  section_item= get_tree_item_by_name(GTK_TREE(SASH_SECURITY_PANE(ssp)->tree), parent_name);
  if (section_item== NULL) {
    section_item= GTK_TREE_ITEM(gtk_tree_item_new_with_label(parent_name));
    gtk_widget_show(GTK_WIDGET(section_item));
    gtk_tree_append(GTK_TREE(SASH_SECURITY_PANE(ssp)->tree), GTK_WIDGET(section_item));
  }
  
  sub_tree= GTK_TREE_ITEM(section_item)->subtree;
  if (sub_tree== NULL) {
    sub_tree= gtk_tree_new();
    gtk_signal_connect(
      GTK_OBJECT(sub_tree),
      (gchar *) "select-child",
      GTK_SIGNAL_FUNC(prohibit_tree_selection), NULL);
    gtk_tree_item_set_subtree(section_item, sub_tree);
  }
  
  item_container= gtk_vbox_new(FALSE, 0);
  
  if (!GTK_IS_TOGGLE_BUTTON(item_widget)) {
    gtk_box_pack_start(GTK_BOX(item_container), item_label, FALSE, TRUE, 0);
    gtk_widget_show(item_label);
  }
  gtk_box_pack_end(GTK_BOX(item_container), item_widget, FALSE, TRUE, 0);
  gtk_widget_show(item_widget);

  entry_item= gtk_tree_item_new();
  gtk_object_set_user_data(GTK_OBJECT(entry_item), value);
  
  gtk_container_add(GTK_CONTAINER(entry_item), item_container);
  gtk_widget_show(item_container);
  gtk_tree_append(GTK_TREE(sub_tree), entry_item);
  gtk_signal_connect(
    GTK_OBJECT(entry_item),
    (gchar *) "select",
    GTK_SIGNAL_FUNC(focus_grabber), item_widget);
  gtk_widget_show(entry_item);
}

gboolean sash_security_pane_set_settings(SashSecurityPane *ssp, GHashTable *security_items) {
  ssp->entry_hash = security_items;
  g_hash_table_foreach(security_items, insert_security_item, ssp);
  gtk_tree_set_view_mode(GTK_TREE(ssp->tree), GTK_TREE_VIEW_ITEM);
  gtk_signal_connect(
    GTK_OBJECT(ssp->tree),
    (gchar *) "select-child",
    GTK_SIGNAL_FUNC(prohibit_tree_selection), NULL);
  return TRUE;
}

void touch_item_settings(SecurityItem *sec, GtkWidget *ui, int to_save) {
  guint i;
  string val;
  gchar *tmp;
  switch (sec->m_Type) {
    case ST_NUMBER:
      if (to_save== 1)
        sec->m_Number= gtk_spin_button_get_value_as_float(GTK_SPIN_BUTTON(ui));
      else
        gtk_spin_button_set_value(GTK_SPIN_BUTTON(ui), sec->m_Number);
      break;
    case ST_STRING:
      if (to_save== 1)
        sec->m_String= gtk_entry_get_text(GTK_ENTRY(ui));
      else
        gtk_entry_set_text(GTK_ENTRY(ui), (gchar *) sec->m_String.c_str());
      break;
    case ST_BOOL:
      if (to_save== 1)
        sec->m_Bool= (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ui))== TRUE) ? true : false;
      else
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ui), (sec->m_Bool ? TRUE : FALSE));
      break;
    case ST_STRING_ENUM:
      if (to_save!= 1) {
        tmp= g_strdup((gchar *) sec->m_StringVals[sec->m_StringIndex].c_str()); 
        gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(ui)->entry), tmp);
        delete [] tmp;
        break;
      }
      val= gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(ui)->entry));
      for (i= 0; i< sec->m_StringVals.size(); ++i)
        if (sec->m_StringVals[i]== val) break;
      if (i>= sec->m_StringVals.size()) {  // THIS IS NOT SUPPOSED TO EVER HAPPEN
        cout << "Serious problem in module SashSecurityPane" << endl;
      }
      else
        sec->m_StringIndex= i;
      break;
    default:
      cout << "Undefined type of attribute: " << sec->m_DescriptiveName << endl;
      break;
  }
}

GtkWidget *get_tree_item_widget(GtkTreeItem *item) {
  GList *list;
  
  list= gtk_container_children(GTK_CONTAINER(GTK_BIN(item)->child));
  list= g_list_next(list);
  return GTK_WIDGET(list->data);
}

// to_save = 1 means OK, all else means REVERT
void iterate_through_items(SashSecurityPane *ssp, int to_save) {
  GList *section, *item;
  GtkTree *sub_tree;
  SecurityItem *sec_item;
  section= gtk_container_children(GTK_CONTAINER(ssp->tree));
  while (section!= NULL) {
    sub_tree= GTK_TREE(GTK_TREE_ITEM_SUBTREE(GTK_TREE_ITEM(section->data)));
    item= gtk_container_children(GTK_CONTAINER(sub_tree));
    while (item!= NULL) {
      sec_item= (SecurityItem *) gtk_object_get_user_data(GTK_OBJECT(item->data));
      touch_item_settings(sec_item, get_tree_item_widget(GTK_TREE_ITEM(item->data)), to_save);
      item= g_list_next(item);
    }
    section= g_list_next(section);
  }
}

gboolean sash_security_pane_ok_settings(SashSecurityPane *ssp) {
  iterate_through_items(ssp, 1);
  return TRUE;
}  

gboolean sash_security_pane_revert_settings(SashSecurityPane *ssp) {
  iterate_through_items(ssp, 0);
  return TRUE;
}

bool sash_security_pane_dialog(GHashTable *security_items, int width, int height) {
  GtkWidget *sashpane, *dialog;
  
  DEBUGMSG(ssp, "Displaying security pane dialog\n");
  dialog = gnome_dialog_new(_("Edit Security Settings"), 
							GNOME_STOCK_BUTTON_OK,
							GNOME_STOCK_BUTTON_CANCEL,
							NULL );
  gnome_dialog_set_default(GNOME_DIALOG(dialog), GNOME_CANCEL);
  gtk_window_set_policy(GTK_WINDOW(dialog), false, true, false);
  gtk_window_set_default_size(GTK_WINDOW(dialog), width, height);
  sashpane = sash_security_pane_new();
  sash_security_pane_set_settings(SASH_SECURITY_PANE(sashpane), security_items);
  gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(dialog)->vbox), sashpane);
  gtk_widget_show_all(dialog);
  int reply = gnome_dialog_run(GNOME_DIALOG(dialog));
  if (reply == GNOME_OK) {
	   DEBUGMSG(ssp, "OK button hit; updating security items\n");
	   sash_security_pane_ok_settings(SASH_SECURITY_PANE(sashpane));
  }
  gtk_widget_destroy(dialog);
  return (reply == GNOME_OK);
}
